# QScrollable

QScrollable is a [QScrollArea](https://doc.qt.io/qt-5/qscrollarea.html) with [QBoxLayout](https://doc.qt.io/qt-5/qboxlayout.html) inside and kinetic scrolling (from [QScroller](https://doc.qt.io/qt-5/qscroller.html)).

Read [the doc](./html/docs/index.html) for more details about the QScrollable methods.

## Install

Open a shell in your Qt project :

```sh
mkdir vendor
cd vendor
git clone https://gitlab.com/GuilleW/QScrollable.git

```

## Example


```cpp
#include "vendor/QScrollable/src/qscrollable.h"

  resize(400,300);
  setStyleSheet("#Item{background-color: rgba(0,0,0,0.2); min-height:60px;}");

  QScrollable *scroll = new QScrollable(QBoxLayout::TopToBottom);
  setCentralWidget(scroll);

  QWidget *w;
  for(int i=0; i<10; i++ ) {
      w = new QWidget();
      w->setObjectName("Item");
      scroll->layout()->addWidget(w);
    }
  scroll->layout()->addStretch();

```

## Contribute
All contributions, code, feedback and strategic advice, are welcome. If you have a question you can contact me directly via email or simply [open an issue](https://gitlab.com/GuilleW/QScrollable/issues/new) on the repository. I'm also always happy for people helping me test new features.
