#ifndef QSCROLLABLE_H
#define QSCROLLABLE_H

#include <QBoxLayout>
#include <QScrollArea>
#include <QScroller>

/**
 * @brief QScrollable is a QScrollArea with QBoxLayout inside and kinetic scrolling.
 */
class QScrollable : public QScrollArea
{
  Q_OBJECT
public:

  /**
   * @brief Constructs a new QScrollable with a QBoxLayout direction and parent widget.
   * @param dir This type is used to determine the direction of a box layout.
   * @param parent widget parent.
   */
  explicit QScrollable(QBoxLayout::Direction dir, QWidget *parent = nullptr);

  /**
   * @brief Returns the layout QBoxLayout that is installed on this widget
   * @return
   */
  QBoxLayout *layout();

private:
  QWidget *w = nullptr;
  QBoxLayout *l = nullptr;

};

#endif // QSCROLLABLE_H
