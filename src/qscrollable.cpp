#include "qscrollable.h"

QScrollable::QScrollable(QBoxLayout::Direction dir, QWidget *parent) : QScrollArea(parent)
{

  setWidgetResizable(true);
  w = new QWidget();
  setWidget(w);

  l = new QBoxLayout(dir);
  w->setLayout(l);

  QScroller::grabGesture(this, QScroller::LeftMouseButtonGesture);

}

QBoxLayout *QScrollable::layout()
{
  return this->l;
}
